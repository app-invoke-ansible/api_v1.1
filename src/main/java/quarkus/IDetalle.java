package quarkus;

import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;



@Entity
public class IDetalle {
     
    //PARAMETROS DE LA CLASE
    
    //DEFINICION DE LA ID
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDetalle;
    
    @NotNull(message = "El componente Detalles no puede ser NULO")
    @NotEmpty(message = "El componente Detalles no puede estar VACIO")
    private String detalles;

    @NotNull(message = "El componente NombrePC no puede ser NULO")
    @NotEmpty(message = "El componente NombrePC no puede estar VACIO")
    private String nombrePC;
    
    @NotNull(message = "El componente NumeroPC no puede ser NULO")
    private int numeroPC;

    //FOREIGN KEY DE PLAYBOOK A DETALLE
    @OneToOne
    @JoinColumn(name = "playbook_id")
    private Playbook playbook;
    
    //FOREIGN KEY DE INVENTARIO A DETALLE
    @OneToOne
    @JoinColumn(name = "inventario_id")
    private Inventario inventario;
    
    //FIN PARAMETROS
    
    /////////////
    
    //GETT AND SETTER

    public Long getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Long idIDetalle) {
        this.idDetalle = idIDetalle;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getNombrePC() {
        return nombrePC;
    }

    public void setNombrePC(String nombrePC) {
        this.nombrePC = nombrePC;
    }

    public int getNumeroPC() {
        return numeroPC;
    }

    public void setNumeroPC(int numeroPC) {
        this.numeroPC = numeroPC;
    }
    

    public Playbook getPlaybook() {
        return playbook;
    }

    public void setPlaybook(Playbook playbook) {
        this.playbook = playbook;
    }

    
    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }
    ////////////////////////7
    
    //EQUALS AND HASH
    @Override
    public int hashCode() {
        return Objects.hash(detalles, nombrePC, numeroPC);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IDetalle other = (IDetalle) obj;
        if (this.numeroPC != other.numeroPC) {
            return false;
        }
        if (!Objects.equals(this.detalles, other.detalles)) {
            return false;
        }
        if (!Objects.equals(this.nombrePC, other.nombrePC)) {
            return false;
        }
        return Objects.equals(this.idDetalle, other.idDetalle);
        
    }
    
    ///////////////////
    
    //toString
    @Override
    public String toString() {
        return "IDetalle{" + "idDetalle=" + idDetalle + ", detalles=" + detalles + ", nombrePC=" + nombrePC + ", numeroPC=" + numeroPC + '}';
    }
}
