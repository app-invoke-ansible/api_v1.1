package quarkus;
import java.util.List;
import java.util.NoSuchElementException;

import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

@Path("/Inventario")
@Authenticated
@Transactional
public class InventarioResource {
    
    //IMPORT REPOSITORY
    @Inject
    private InventarioRepository inventarioRepository;

    //METODO GET
    @GET
    public List<Inventario> index(){
        return inventarioRepository.listAll();
    }

    //METODO POST
    @POST
    public Inventario insert(@Valid Inventario inventario){
        inventarioRepository.persist(inventario);
        return inventario;
    }

    //METODO BUSQUEDA POR ID
    @GET
    @Path("{idInventario}")
    public Inventario retrieve(@PathParam("idInventario") Long id){
        var inventario = inventarioRepository.findById(id);
        if (inventario != null){
            return inventario;
        }
        throw new NoSuchElementException("No existe Inventario con esta id" +id+".");
    }

    //METODO DELETE
    @DELETE
    @Path("{idInventario}")
    public String delete(@PathParam("idInventario") Long id){
        if (inventarioRepository.deleteById(id)){
            return "Inventario eliminado";
        }
        else{
            return "no se encontro Inventario ah eliminar";
        }
    }

    //METODO PUTT
    @PUT
    @Path ("{idInventario}")
    public Inventario update(@PathParam("idInventario")@Valid Long id, Inventario inventario){
        var updateInventario = inventarioRepository.findById(id);
        if (updateInventario != null){
            updateInventario.setNombreInventario(inventario.getNombreInventario());
            updateInventario.setAnsibleInventario(inventario.getAnsibleInventario());
        }
        throw new NoSuchElementException("No existe Inventario de id"+ id+ "para editar.");
    }
}
