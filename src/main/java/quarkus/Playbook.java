package quarkus;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;


import java.util.Objects;


@Entity
public class Playbook {
    
    //PARAMETROS DE LA CLASE

    //DEFINICION DE ID
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPlaybook;

    @NotNull(message = "ERROR revise el campo de Nombre")
    @NotEmpty(message = "El nombre del Playbook no puede estar VACIO")
    private String nombrePlaybook;

    @NotNull(message = "ERROR revise el campo de Ansible")
    private int ansiblePlaybook;

    //FOREING KEY DE MODULO A PLAYBOOK
    @NotNull(message = "debes tener un modulo a vincular")
    @ManyToOne
    @JoinColumn(name = "modulo_id") //Nombre de columna en la tabla que usara la FK
    private Modulo modulo;

    //FOREING KEY DE PLAYBOOK A DETALLE
    @OneToOne(mappedBy = "playbook", cascade = CascadeType.ALL, orphanRemoval = true) //Nombre del campo en la clase playbook que mapea la relacion
    private IDetalle detalle;
    
    //FIN DE PARAMETROS DE CLASE

    ///////////////////

    //GETT AND SETTERS DE LAS CLASES

    public Long getIdPlaybook() {
        return idPlaybook;
    }

    public void setIdPlaybook(Long idPlaybook) {
        this.idPlaybook = idPlaybook;
    }

    public String getNombrePlaybook() {
        return nombrePlaybook;
    }

    public void setNombrePlaybook(String nombrePlaybook) {
        this.nombrePlaybook = nombrePlaybook;
    }

    public int getAnsiblePlaybook() {
        return ansiblePlaybook;
    }

    public void setAnsiblePlaybook(int ansiblePlaybook) {
        this.ansiblePlaybook = ansiblePlaybook;
    }
    
    public Modulo getModulo() {
        return modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }
    //FIN GETT AND SETTERS
    
    //////////////////////
    
    //EQUALS AND HASH

    @Override
    public int hashCode() {
       return Objects.hash(nombrePlaybook, ansiblePlaybook);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Playbook other = (Playbook) obj;
        if (this.ansiblePlaybook != other.ansiblePlaybook) {
            return false;
        }
        if (!Objects.equals(this.nombrePlaybook, other.nombrePlaybook)) {
            return false;
        }
        return Objects.equals(this.idPlaybook, other.idPlaybook);
    }
    
    
    /////////////////////
    //TO STRING

    @Override
    public String toString() {
        return "Playbook{" + "idPlaybook=" + idPlaybook + ", nombrePlaybook=" + nombrePlaybook + ", ansiblePlaybook=" + ansiblePlaybook + ", modulo=" + modulo + ", detalle=" + detalle + '}';
    }
    
    
}
