package quarkus;

import java.util.Optional;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.validation.Validator;

@ApplicationScoped
public class Validador {
    

    Validator validator;

    @Inject
    public Validador(Validator validator) {
        this.validator = validator;
    }

    public Optional<String>validatePlaybooks(Playbook playbook){
        var errors = validator.validate(playbook);
        if (errors.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(errors.stream().findFirst().get().getMessage());
    }
}
