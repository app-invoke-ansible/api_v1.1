package quarkus;

import java.util.List;
import java.util.NoSuchElementException;

import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

@Path("/Modulo")
@Transactional
@Authenticated
public class ModuloResouce {
    
    //IMPORT DE REPOSITORY
    @Inject
    private ModuloRepository moduloRepository;

    //METODO GET
    @GET
    public List<Modulo> index(){
        return moduloRepository.listAll();
    }

    //METODO POST
    @POST
    public Modulo insert(@Valid Modulo modulo){
        moduloRepository.persist(modulo);
        return modulo;
    }

    //METODO BUSCAR POR ID
    @GET
    @Path("{idModulo}")
    public Modulo retrieve(@PathParam("idModulo") Long id){
        var modulo = moduloRepository.findById(id);
        if (modulo != null){
            return modulo;
        }
        throw new NoSuchElementException("No existe Modulo con esta id" +id+".");
    }

    //METODO DELETE
    @DELETE
    @Path("{idModulo}")
    public String delete(@PathParam("idModulo") Long id){
        if (moduloRepository.deleteById(id)){
            return "Modulo eliminado";
        }
        else{
            return "no se encontro Modulo ah eliminar";
        }
    }

    //METODO PUT
    @PUT
    @Path ("{idModulo}")
    public Modulo update(@PathParam("idModulo") Long id,@Valid Modulo modulo){
        var updateModulo = moduloRepository.findById(id);
        if (updateModulo != null){
            updateModulo.setNombreModulo(modulo.getNombreModulo());
        }
        throw new NoSuchElementException("No existe Modulo de id"+ id+ "para editar.");
    }
}
