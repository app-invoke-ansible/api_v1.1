package quarkus;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import jakarta.persistence.OneToMany;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

@Entity
public class Modulo {
    
    //PARAMETROS DE LA CLASE
    
    //DEFINICION ID
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idModulo;
    
    @NotNull (message = "El nombre del MODULO no puede ser NULO")
    @NotEmpty (message = "EL nombre del MODULO no puede ir VACIO")
    private String nombreModulo;

    @NotNull(message = "El estado del MODULO no puede ser NULO")
    @NotEmpty(message = "El estado del MODULO no puede estar VACIO")
    private String estadoModulo;
    
    @NotNull(message = "La operacion del MODULO no puede ser NULO")
    @NotEmpty(message = "La operacion del MODULO no puede estar VACIO")
    private String operacionModulo;

    @CreationTimestamp
    private LocalDate fechaModulo;

    //FOREIGN KEY DE MODULO A PLAYBOOK
    @OneToMany(mappedBy = "modulo")
    private List<Playbook> playbooks;
    
    //FOREIGN KEY DE MODULO A INVENTARIO 
    @OneToMany(mappedBy = "modulo")
    private List<Inventario> inventarios;
    
    //FIN DE PARAMETROS
    
    ////////////////////
    
    //GETT AND SETTER

    public Long getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(Long idModulo) {
        this.idModulo = idModulo;
    }

    public String getNombreModulo() {
        return nombreModulo;
    }

    public void setNombreModulo(String nombreModulo) {
        this.nombreModulo = nombreModulo;
    }

    public String getEstadoModulo() {
        return estadoModulo;
    }

    public void setEstadoModulo(String estadoModulo) {
        this.estadoModulo = estadoModulo;
    }

    public String getOperacionModulo() {
        return operacionModulo;
    }

    public void setOperacionModulo(String operacionModulo) {
        this.operacionModulo = operacionModulo;
    }

    public LocalDate getFechaModulo() {
        return fechaModulo;
    }

    public void setFechaModulo(LocalDate fechaModulo) {
        this.fechaModulo = fechaModulo;
    }
    
    //////////////////////////
    
    //EQUALS AND HASH


    @Override
    public int hashCode() {
        return Objects.hash(nombreModulo, estadoModulo, operacionModulo, fechaModulo);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Modulo other = (Modulo) obj;
        if (!Objects.equals(this.nombreModulo, other.nombreModulo)) {
            return false;
        }
        if (!Objects.equals(this.estadoModulo, other.estadoModulo)) {
            return false;
        }
        if (!Objects.equals(this.operacionModulo, other.operacionModulo)) {
            return false;
        }
        if (!Objects.equals(this.idModulo, other.idModulo)) {
            return false;
        }
        return Objects.equals(this.fechaModulo, other.fechaModulo);
    }
    
    
    ///////////////////////////
    
    //TOSTRING

    @Override
    public String toString() {
        return "Modulo{" + "idModulo=" + idModulo + ", nombreModulo=" + nombreModulo + ", estadoModulo=" + estadoModulo + ", operacionModulo=" + operacionModulo + ", fechaModulo=" + fechaModulo + '}';
    }
    
    
}
