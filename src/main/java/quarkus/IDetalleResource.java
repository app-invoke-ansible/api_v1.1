package quarkus;

import java.util.List;
import java.util.NoSuchElementException;

import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

@Path("/IDetalle")
@Authenticated
@Transactional
public class IDetalleResource {
    
    //IMPORT REPOSITORY
    @Inject
    private IDetalleRepository iDetalleRepository;

    //METODO GET
    @GET
    public List<IDetalle> index(){
        return iDetalleRepository.listAll();
    }

    //METODO POST
    @POST
    public IDetalle insert(@Valid IDetalle iDetalle){
        iDetalleRepository.persist(iDetalle);
        return iDetalle;
    }

    //METODO BUSCAR POR ID
    @GET
    @Path("{idInvDetalle}")
    public IDetalle retrieve(@PathParam("idInvDetalle") Long id){
        var iDetalle = iDetalleRepository.findById(id);
        if (iDetalle != null){
            return iDetalle;
        }
        throw new NoSuchElementException("No existe Detalles con esta id" +id+".");
    }

    //METODO METODO DELETE
    @DELETE
    @Path("{idInvDetalle}")
    public String delete(@PathParam("idInvDetalle")@Valid Long id){
        if (iDetalleRepository.deleteById(id)){
            return "Detalle eliminado";
        }
        else{
            return "no se encontro Detalle ah eliminar";
        }
    }

    //METODO PUT
    @PUT
    @Path ("{idInvDetalle}")
    public IDetalle update(@PathParam("idInvDetalle") Long id, IDetalle iDetalle){
        var updateInvDetalle = iDetalleRepository.findById(id);
        if (updateInvDetalle != null){
            updateInvDetalle.setDetalles(iDetalle.getDetalles());
            updateInvDetalle.setNombrePC(iDetalle.getNombrePC());
            updateInvDetalle.setNumeroPC(iDetalle.getNumeroPC());
        }
        throw new NoSuchElementException("No existe Detalle de id"+ id+ "para editar.");
    }
}
