
package quarkus;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

import java.util.Objects;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;



@Entity
public class Inventario {
  
    //PARAMETROS DE LA CLASE
    
    //DEFINICION DE LA ID
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idInventario;
    
    @NotNull (message = "El Campo nombre no puede ser de valor NULO")
    @NotEmpty ( message = "El Campo nombre no puede estar VACIO")
    private String nombreInventario;
    
    @NotNull (message = "El campo Ansible no puede ser de valor NULO")
    private int ansibleInventario;
    
    //FOREIGN KEY DE MODULO A INVENTARI
    @ManyToOne
    @JoinColumn(name = "modulo_id") 
    private Modulo modulo;
    
    //FOREIGN KEY DE INVENTARIO A DETALLE
    @OneToOne(mappedBy = "inventario", cascade = CascadeType.ALL, orphanRemoval = true)
    private IDetalle detalle;
    
    //FIN PARAMETROS DE LA CLASE
    
    /////////////////////////////
    
    //GETT AND SETTER PARAMETROS

    public Long getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Long idInventario) {
        this.idInventario = idInventario;
    }

    public String getNombreInventario() {
        return nombreInventario;
    }

    public void setNombreInventario(String nombreInventario) {
        this.nombreInventario = nombreInventario;
    }

    public int getAnsibleInventario() {
        return ansibleInventario;
    }

    public void setAnsibleInventario(int ansibleInventario) {
        this.ansibleInventario = ansibleInventario;
    }
    
    public Modulo getModulo() {
        return modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }
    
    //////////////////////////////
    
    //EQUALS AND HASH
    
    @Override
    public int hashCode() {
        return Objects.hash(nombreInventario, ansibleInventario);
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Inventario other = (Inventario) obj;
        if (this.ansibleInventario != other.ansibleInventario) {
            return false;
        }
        if (!Objects.equals(this.nombreInventario, other.nombreInventario)) {
            return false;
        }
        return Objects.equals(this.idInventario, other.idInventario);
    }
    
    
    
    //////////////////////////////////////
    
    //toString

    @Override
    public String toString() {
        return "Inventario{" + "idInventario=" + idInventario + ", nombreInventario=" + nombreInventario + ", ansibleInventario=" + ansibleInventario + ", modulo=" + modulo + ", detalle=" + detalle + '}';
    }

    
   
}
