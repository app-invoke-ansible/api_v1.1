package quarkus;

import java.util.List;
import java.util.NoSuchElementException;


import io.quarkus.security.Authenticated;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

@Path("/Playbooks")
@Transactional
@Authenticated
public class PlaybookResource {
    
    //IMPORT DE REPOSITORY
    @Inject
    private PlaybookRepository playbookRepository;

    //METODO GET
    @GET
    public List<Playbook> index(){
        return playbookRepository.listAll();
    }

    //METODO POST
    @POST
    public Playbook insert(@Valid Playbook playbook){
        playbookRepository.persist(playbook);
        return playbook;
    }

   /*   
        @POST
        public Playbook insert(Playbook playbook){
            var error = this.validador.validatePlaybook(playbook);
            if(error.isPresent()){
                var msg = error.get();
                return Response.status(400).entity(msg).build();
            }
            playbookRepository.persist(playbook);
            return playbook;
        } 
    */

    //METODO BUSCAAR POR ID
    @GET
    @Path("{idPlaybook}")
    public Playbook retrieve(@PathParam("idPlaybook") Long id){
        var playbook = playbookRepository.findById(id);
        if (playbook != null){
            return playbook;
        }
        throw new NoSuchElementException("No existe Playbook con esta id" +id+".");
    }

    //METODO DELETE
    @DELETE
    @Path("{idPlaybook}")
    public String delete(@PathParam("idPlaybook") Long id){
        if (playbookRepository.deleteById(id)){
            return "Playbook eliminado";
        }
        else{
            return "no se encontro Playbook ah eliminar";
        }
    }

    //METODO PUT
    @PUT
    @Path ("{idPlaybook}")
    public Playbook update(@PathParam("idPlaybook") Long id,@Valid Playbook playbook){
        var updatePlaybook = playbookRepository.findById(id);
        if (updatePlaybook != null){
            updatePlaybook.setNombrePlaybook(playbook.getNombrePlaybook());
            updatePlaybook.setAnsiblePlaybook(playbook.getAnsiblePlaybook());
        }
        throw new NoSuchElementException("No existe playbook de id"+ id+ "para editar.");
    }
}
